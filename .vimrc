"#####表示設定#####
syntax on "コードの色分け
colorscheme hybrid "色設定
set t_Co=256
set guifont=rictyDiscord-Regular:h9 "フォント?
"set encoding=utf-8 "デフォルトの文字コード
"scriptencoding utf-8
"set fileencoding=utf-8
"set number "行番号を表示する

:set enc=utf-8
:set fenc=utf-8
:set fencs=utf-8,iso-2022-jp,euc-jp,cp932

set ruler "カーソルの位置をVim最下部のバーに表示する
set title "編集中のファイル名を表示
set showmatch "括弧入力時の対応する括弧を表示
set shiftwidth=4 "自動インデントでずれる幅
set smartindent "改行時に入力された行の末尾に合わせて次の行のインデントを増減する
set autoindent "改行時に前の行のインデントを継続する
set showmode "モード表示する
set list
highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=#666666
au BufNewFile,BufRead * match ZenkakuSpace /　/ "全角スペースを可視化
set listchars=tab:>-,trail:_,eol:↩︎ 
set listchars=extends:>,precedes:<,nbsp:% "タブ空白改行の可視化
set expandtab "タブを空白で入力
set tabstop=4 "インデント(標準タブ)をスペース2つ分に設定
set softtabstop=4 "連続した空白に対してタブキーやバックスペースキーでカーソルが動く幅

"#####検索設定#####
set ignorecase "大文字/小文字の区別なく検索する
set smartcase "検索文字列に大文字が含まれている場合は区別して検索する
set wrapscan "検索時に最後まで行ったら最初に戻る

